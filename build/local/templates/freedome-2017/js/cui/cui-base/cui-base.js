/*
 Сборник сниппетов cui
 var selector - селектор jquery
 var $el - объект jquery
 */

var cui = new function(){
    var c = this;
    //клик вне областей selector.
    c.clickOff = function ( $el, callback ) {
        if( !$el ) return false;
        $(document).on('click', function (e) {
            if ($el.has(e.target).length === 0 && !$el.is(e.target)){
                callback($el);
            }
        });
    };
    //вызывает события элемента touchmoveStart, touchmoveEnd, scrollStart, scrollEnd когдо доскролили до начала/конца
    c.scrollEvents = function ( selector ){
        var $el = $(selector);
        var startCoordY = 0;
        var isTouching = false;

        $(document)
            .on('touchstart', selector, function(event) {
                startCoordY = event.touches[0]["pageY"];
                isTouching = true;
            })
            .on('touchmove', selector, function(event) {
                var isStart = $el.scrollTop() == 0;
                var isEnd = ($el.innerHeight() ^ 0) >= $el.get(0).scrollHeight || ($el.scrollTop()+$el.innerHeight()) == $el.get(0).scrollHeight;

                var scrollCoordY = event.touches[0]["pageY"];
                var isDirectionBot = (startCoordY - scrollCoordY ) > 0;

                if( isEnd && isDirectionBot ) $el.trigger('touchmoveEnd', event);
                if( isStart && !isDirectionBot ) $el.trigger('touchmoveStart', event);
            })
            .on('touchend', selector, function(event) {
                isTouching = false;
            });
        $el
            .on('scroll', function(event) {
                if( isTouching ) return;

                var isStart = $el.scrollTop() == 0;
                var isEnd = ($el.innerHeight() ^ 0) >= $el.get(0).scrollHeight || ($el.scrollTop()+$el.innerHeight()) == $el.get(0).scrollHeight;

                if (isStart) $el.trigger( "scrollStart", event);
                if (isEnd) $el.trigger( "scrollEnd", event);
            });
        return $el;
    };
    //парсим гет параметры в объект
    c._GET = function () {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = decodeURIComponent(value);
        });
        return vars;
    };
    c.ifdefined = function ( v ) {
        return typeof v !== 'undefined' && v !== null;
    };
    c.replaceAll = function (search, replacement) {
        var t = this;
        [].concat(search).forEach(function (item, i) {
            var replace = Array.isArray(replacement) ? replacement[i] || "" : replacement || "";
            t = t.replace(new RegExp("["+item+"]", 'g'), replace);
        });
        return t;
    };
    //срабатывает когда загружена картинка или массив картинок
    c.onImageLoaded = function ($el, callback) {
        if ( typeof $el == "undefined" ) {console.warn("Пустой аргумент"); return;}

        var callback = callback || function(){};
        var count = $el.length;

        var checkComplete = function (inc) {
            var inc = inc || 0;
            count += inc;
	        if(!count) callback();
        };
	    checkComplete();

        $el.each(function(index, item) {
	        if (item.complete) checkComplete(-1);

	        $(item).on("load", function(){
		        checkComplete(-1);
	        });
        });
    };

};

