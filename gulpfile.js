var gulp = require("gulp");
var rigger = require("gulp-rigger");
var pagebuilder = require('gulp-file-include');

var path = {
    src: {
        html: 'src/*.html',
        js: 'src/js/*.js'
    },
    build: {
        html: 'build',
        js: 'build/assets/js'
    }
}

gulp.task('inject', function () {
    var htmlInject = gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(pagebuilder({
            prefix: '@@',
            basepath: '@file'
          }))
        .pipe(gulp.dest(path.build.html));

    var jsInject = gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.js));
});

gulp.task('watch', ['inject'], function () {
    gulp.watch('src/**/*', ['inject']);
});

gulp.task('default', ['watch']);