// BEGIN: object
$(document).ready(function () {
    $('body').on('click', '.obj-section_link-map', function () {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 49
        }, 1000);
        event.preventDefault();
    });
    $('body').on('click', '.obj-card_value .dropdown__item', function () {
        var drop = $(this).parent().parent();
        var menu = drop.find('.dropdown__menu');
        var inp = menu.find('input[type=radio]');
        var arr = [];
        $(inp).each(function () {
            if ($(this).prop('checked') == true) {
                arr.push($(this).val());
            }
        });
        if (arr.length > 0) {
            drop.find('.name').text(arr);
        } else if (arr.length == 0) {
            drop.find('.value').text('');
        }
    });
    $('body').on('keyup change blur click', '.input-box[type=text], input[type=checkbox]', function () {
        if ($(this).attr('name') == 'phoneAdvert') {
            var phone = $(this);
            checkPhone(phone);
        }
        checkForm($(this.form));
    });
    
});

function toggleHeight(el, btn) {
    $(btn).children().toggleClass('hide');
    $(el).toggleClass('show');
}


// END: object