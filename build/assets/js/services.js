// BEGIN: services
$(document).ready(function () {
    $('body').on('click', '[data-toggle]', function (e) {
        e.preventDefault();
        var el = $(this);
        var printBtn = $(this).parent().find('.srvs-price_print');
        $(el).next('[data-attr="tab"]').slideToggle('slow')
            .parent()
            .siblings()
            .find('[data-attr="tab"]:visible')
            .slideUp('slow');
        el.toggleClass('active');
        el.parent()
            .siblings()
            .find('[data-toggle]').removeClass('active');
        printBtn.toggleClass('active');
        printBtn.parent()
            .siblings()
            .find('.srvs-price_print').removeClass('active');
    });
    //calcCredit

    $(".calc_range").each(function () {
        var inp = $(this).siblings('input');
        var key = $(this).attr('data-key');
        var start = inp.attr('value'),
            min = parseInt($(this).attr('min'), 10),
            max = parseInt($(this).attr('max'), 10);

        if (key) {
            inp.val(start.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + key);
        }
        $(this).slider({
            value: start,
            orientation: "horizontal",
            range: "min",
            min: min,
            max: max,
            animate: true,
            slide: function (event, ui) {
                $(inp).val(ui.value.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + key);

                $('[data-attr="permonth"] .value').text(Math.round(result()).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));

                initial();
            },
            change: function (event, ui) {
                $(inp).val(ui.value.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + key);

                $('[data-attr="permonth"] .value').text(Math.round(result()).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));

                $('#permonth').val(Math.round(result()));

                initial();
            }
        });
    });
    //шаг для ползунка процентов
    $('[data-attr="per"]').siblings('.calc_range').slider({
        step: 0.5
    });
    //вывод ежемесячного платежа
    $('[data-attr="permonth"] .value').text(Math.round(result()).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
    $('#permonth').val(Math.round(result()));

    initial();
    currencyToggle();
    //изменение значения ползунка при вводе значения в инпут
    var inp = document.querySelectorAll('.calc_data .val input[type=text]');
    inp.forEach(function (elem, i, inp) {
        elem.addEventListener("change", function () {
            $(this).siblings('.calc_range').slider('value', parseInt($(this).val(), 10));
        });
    });
    //расчёт ежемесячного платежа
    function result() {
        var credit = $('[data-attr="credit"]').siblings('.calc_range').slider('value'),
            per = $('[data-attr="per"]').siblings('.calc_range').slider('value'),
            term = $('[data-attr="term"]').siblings('.calc_range').slider('value');
        var month = term * 12;
        var monthly = (credit * term + credit * (per / 100)) / month;
        return monthly;
    }

    function initial() {
        $('[data-attr="creditResult"] .value').text(Math.round($('[data-attr="credit"]').siblings('.calc_range').slider('value')).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
        $('#creditResult').val(parseInt($('[data-attr="credit"]').siblings('.calc_range').slider('value')));

        $('[data-attr="initialFeeResult"] .value').text(Math.round($('[data-attr="initialFee"]').siblings('.calc_range').slider('value')).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
        $('#initialFeeResult').val(parseInt($('[data-attr="initialFee"]').siblings('.calc_range').slider('value')));
    }

    function currencyToggle() {
        var inp = $('input[name="currency"]');
        inp.on("change", function () {
            $('.calc_result .val .cur').text($(this).attr('data-key'));
            $('.val_cur').attr('data-key', $(this).attr('data-key'));
        });
    }
    //radio click
    $('body').on('click', '.radio-btn', function () {
        $(this).siblings('input[type=radio]').click();
    });
    //отправка на расчёт
    $("form").submit(function () {
        $.post(
            $(this).attr('action'),
            $(this).serialize()
        );
    });
    //calc mask
    $(".calc_data .val input[type=text]").on('click', function () {
        var curVal = $(this).val();
        $(this).select();
    });
    $(".calc_data .val input[type=text]").keypress(function (e) {
        if (e.which == 13) {
            $(".calc_data .val input[type=text]").blur();
            return false;
        }
        //запрет ввода букв
        if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    //открытие попапа с заявкой
    $('.btn-action:not(.btn-action--view):not(.btn-action--steps)').magnificPopup({
        closeMarkup: '<button title="%title%" type="button" class="mfp-close popup__close"></button>'
    });
    // отправить мне на email
    $('.services-footer label').click(function () {
        $(this).siblings('input').click();
    });
    //проверка формы
    $('body').on('keyup change blur click', '.srvs-demand_tab input[type=text], .srvs-demand_tab input[type=checkbox]', function () {
        var form = $(this.form);
        if ($(this).attr('type') == 'text') {
            var email = $(this);
            checkEmail(email);
        }
        formCheck(form);
    });
    $('body').on('keyup change blur click', '#formRequest input[type=text], #formRequest input[type=checkbox]', function () {
        var form = $(this.form);
        formCheck(form);
    });
    //ипотека выпадашка
    $('body').on('click', '.calc .dropdown__item', function () {
        var drop = $(this).parent().parent();
        var menu = drop.find('.dropdown__menu');
        var inp = menu.find('input[type=radio]');
        var arr = [];
        $(inp).each(function () {
            if ($(this).prop('checked') == true) {
                arr.push($(this).val());
            }
        });
        if (arr.length > 0) {
            drop.find('.name').text(arr);
        } else if (arr.length == 0) {
            drop.find('.value').text('');
        }
    });
    //anchor price
    $('body').on('click', '.btn-action--view, .btn-action--steps', function () {
        event.preventDefault();
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 200
        }, 1000);
    });
});

//print prices
function addPrint(el) {
    el.parents('.srvs-price_item').siblings('.srvs-price_item').removeClass('print-wrap');
    el.parents('.srvs-price_item').addClass('print-wrap');
    print();
}

function formCheck(form) {
    if (!(form.find("input[type=text], input[type=email]").hasClass('error')) && !(form.find("input[type=text], input[type=email]").val() == '') && form.find("input[type=checkbox]").prop("checked") == true) {
        form.find("[type=submit]").addClass("active");
        form.find("[type=submit]").prop("disabled", false);
        return true;
    } else {
        form.find("[type=submit]").removeClass("active");
        form.find("[type=submit]").prop("disabled", true);
    }
}
// END: services